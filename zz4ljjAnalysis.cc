// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/FastJets.hh"

namespace Rivet {
  
  /// VBFZ in pp at 13 TeV
  class zz4ljjAnalysis : public Analysis {
  public:
 

    /// Constructor
    RIVET_DEFAULT_ANALYSIS_CTOR(zz4ljjAnalysis);

    /// Book histograms and initialise projections before the run
    void init() {
      FinalState fs(Cuts::abseta < 5.0);

      PromptFinalState photons(Cuts::abspid == PID::PHOTON);
      PromptFinalState electrons(Cuts::abspid == PID::ELECTRON);
      PromptFinalState muons(Cuts::abspid == PID::MUON);

      Cut cuts_el = (Cuts::pT > 7*GeV) && ( Cuts::abseta < 2.5);
      Cut cuts_mu = (Cuts::pT > 5*GeV) && ( Cuts::abseta < 2.7);

      DressedLeptons dressed_electrons(photons, electrons, 0.1, cuts_el);
      declare(dressed_electrons, "DressedElectrons");

      DressedLeptons dressed_muons(photons, muons, 0.1, cuts_mu);
      declare(dressed_muons, "DressedMuons");

      FastJets jets(fs, FastJets::ANTIKT, 0.4, JetAlg::Muons::NONE, JetAlg::Invisibles::NONE);
      declare(jets, "Jets");
 
      //book plots 
      book(mainPlot.m_jj,    1 , 1, 1);
      book(mainPlot.Dy_jj,   2 , 1, 1);
      book(mainPlot.Dphi_jj, 3 , 1, 1);
      book(mainPlot.pT_ll,   4 , 1, 1);
      book(mainPlot.pT_ll_2, 5 , 1, 1);
      book(mainPlot.m_ll,    6 , 1, 1);
      book(mainPlot.m_ll_2,  7 , 1, 1);
    }

    /// Perform the per-event analysis
    void analyze(const Event& event) {

      // access fiducial electrons and muons
      const Particle *l1 = nullptr, *l2 = nullptr;
      const Particle *l3 = nullptr, *l4 = nullptr;
      auto muons = apply<DressedLeptons>(event, "DressedMuons").dressedLeptons();
      auto elecs = apply<DressedLeptons>(event, "DressedElectrons").dressedLeptons();

      // 4l selection 1: at least 4 leptons in the event
      if (muons.size()+elecs.size()<4) vetoEvent;

      // 4l selection 2: enough postive and negtive pairs!
      vector<DressedLepton> leptonsFS_sel4l;
      leptonsFS_sel4l.insert( leptonsFS_sel4l.end(), muons.begin(), muons.end() );
      leptonsFS_sel4l.insert( leptonsFS_sel4l.end(), elecs.begin(), elecs.end() );
   
      size_t el_p = 0;
      size_t el_n = 0;
      size_t mu_p = 0; 
      size_t mu_n = 0;
      
      for (const Particle& l : leptonsFS_sel4l) {
        if (l.abspid() == PID::ELECTRON) {
          if (l.pid() < 0)  ++el_n;
          if (l.pid() > 0)  ++el_p;
        }
        else if (l.abspid() == PID::MUON) {
          if (l.pid() < 0)  ++mu_n;
          if (l.pid() > 0)  ++mu_p;
        }
      }

      bool pass_sfos = ( (el_p >=2 && el_n >=2) || (mu_p >=2 && mu_n >=2) || (el_p >=1 && el_n >=1 && mu_p >=1 && mu_n >=1) );
      
      if (!pass_sfos)  vetoEvent;

      //4l selection 3: the leading and sub-leading leptons of the event must have pt>20 GeV
      
      std::vector<double> lepton_pt;
      for (const Particle& i : leptonsFS_sel4l) lepton_pt.push_back(i.pT() / GeV);
      std::sort(lepton_pt.begin(), lepton_pt.end(), [](const double pT1, const double pT2) { return pT1 > pT2; });
      
      if (!(lepton_pt[0] > 20*GeV && lepton_pt[1] > 20*GeV))  vetoEvent;

      //4l selection 4: any l+l- pair must have mll>5 GeV
      size_t n_parts = leptonsFS_sel4l.size();
      for (size_t i = 0; i < n_parts - 1; ++i) {
        for (size_t j = i + 1; j < n_parts; ++j) {


          Zstate twol = Zstate( ParticlePair(leptonsFS_sel4l[i], leptonsFS_sel4l[j]) );
          double mass_twol = twol.mom().mass()/GeV;

          if (mass_twol < 5*GeV) vetoEvent;
        }
      }

      //ZZ selection: select the two l+l- pairs with mass closest to mZ
      Zstate Z1, Z2, Zcand;
      size_t l1_index = 0;
      size_t l2_index = 0; 

    
      // determine Z1 first
      double min_mass_diff = -1;
      for (size_t i = 0; i < n_parts - 1; ++i) {
        for (size_t j = i + 1; j < n_parts; ++j) {

          if (leptonsFS_sel4l[i].pid() != -1*leptonsFS_sel4l[j].pid())  continue; //only pair SFOS leptons

          Zcand = Zstate( ParticlePair(leptonsFS_sel4l[i], leptonsFS_sel4l[j]) );
          double mass_diff = fabs( Zcand.mom().mass() - 91.1876 );
         
          if (min_mass_diff == -1 || mass_diff < min_mass_diff) {
            min_mass_diff = mass_diff;
            Z1 = Zcand;
            l1_index = i;
            l2_index = j;
          }
        }
      }

      //determine Z2 second
      min_mass_diff = -1;
      for (size_t i = 0; i < n_parts - 1; ++i) {
        if (i == l1_index || i == l2_index)  continue;
        for (size_t j = i + 1; j < n_parts; ++j) {
          if (j == l1_index || j == l2_index)  continue;

          if (leptonsFS_sel4l[i].pid() != -1*leptonsFS_sel4l[j].pid())  continue; // only pair SFOS leptons

          Zcand = Zstate( ParticlePair(leptonsFS_sel4l[i], leptonsFS_sel4l[j]) );
          double mass_diff = fabs( Zcand.mom().mass() - 91.1876 );

          if (min_mass_diff == -1 || mass_diff < min_mass_diff) {
            min_mass_diff = mass_diff;
            Z2 = Zcand;
          }
        }
      }

      Particles leptons_sel4l;
      leptons_sel4l.push_back(Z1.first);
      leptons_sel4l.push_back(Z1.second);
      leptons_sel4l.push_back(Z2.first);
      leptons_sel4l.push_back(Z2.second);


      //4l selection : all leptons must be separated by at least deltaR=0.05
      for (unsigned int i = 0; i < 3; ++i) {
        for (unsigned int j = i + 1; j < 4; ++j) {
          double dR = deltaR(leptons_sel4l[i], leptons_sel4l[j]);

          if (dR < 0.05)  vetoEvent;
        }
      }  

      //zz selection: m4l>100GeV
      FourMomentum FourL = Z1.mom() + Z2.mom();
      double M4l = FourL.mass()/GeV;
      if (M4l < 100*GeV) vetoEvent;


      //Jet selection: jets must have pt>15 GeV and |eta|<4.5 
      Jets jets;
      for (const Jet& j : apply<FastJets>(event, "Jets").jetsByPt(Cuts::pT > 15*GeV && Cuts::absrap < 4.5)) {
        jets += j;
      }

      //Jet selection looser selection for the dijet system: I require the two jets to have pt>(40,30) GeV, mjj>200 GeV and no eta1*eta2 cut, dY>2
      if (jets.size() < 2) vetoEvent;

      // Calculate the observables
      l1=&leptons_sel4l[0];
      l2=&leptons_sel4l[1];
      l3=&leptons_sel4l[2];
      l4=&leptons_sel4l[3];
      Variables vars(jets, l1, l2, l3, l4);


      if (jets[0].pt() < 40*GeV || jets[1].pt() < 30*GeV ) vetoEvent;

      bool dijetcuts_loose = (vars.mjj > 200*GeV && vars.Dyjj > 2.0);

      if (!dijetcuts_loose) vetoEvent;
  
      //Pass and fill!!! 

      fillPlots(vars, mainPlot);
    }

    void finalize() {
      const double xsec = crossSectionPerEvent()/femtobarn;
      scalePlots(mainPlot, xsec);
      std::cout << "Done!" << '\n';
    }
 
    struct Zstate : public ParticlePair {
      Zstate() { }
      Zstate(ParticlePair _particlepair) : ParticlePair(_particlepair) { }
      FourMomentum mom() const { return first.momentum() + second.momentum(); }
      operator FourMomentum() const { return mom(); }
    };


    struct Variables {
      Variables(const vector<Jet>& jets, const Particle* l1, const Particle* l2, const Particle* l3, const Particle* l4) {
        // get the jets
        assert(jets.size()>=2);
        FourMomentum j1 = jets[0].mom(), j2 = jets[1].mom();
        pTj1 = j1.pT()/GeV; pTj2 = j2.pT()/GeV;
        assert(pTj1 >= pTj2);
        
        // build 2 dilepton system
        FourMomentum ll = (l1->mom() + l2->mom());
        pTll = ll.pT(); mll = ll.mass();
        
        FourMomentum ll_2 = (l1->mom() + l2->mom());
        pTll_2 = ll_2.pT(); mll_2 = ll.mass();
        
        Nj = jets.size();
        Dyjj = std::abs(j1.rap() - j2.rap());
        mjj = (j1 + j2).mass();
        Dphijj = ( j1.rap() > j2.rap() ) ? mapAngleMPiToPi(j1.phi() - j2.phi()) : mapAngleMPiToPi(j2.phi() - j1.phi());
        
        Jets gjets = getGapJets(jets);
        Ngj = gjets.size();
        pTgj = Ngj? gjets[0].pT()/GeV : 0;
        
        FourMomentum vecSum = (j1 + j2 + l1->mom() + l2->mom());
        double HT = j1.pT() + j2.pT() + l1->pT() + l2->pT();
        if (Ngj) { 
          vecSum += gjets[0].mom(); 
          HT += pTgj;
        }
        pTbal = vecSum.pT() / HT;
        
        Zcent = std::abs(ll.rap() - (j1.rap() + j2.rap())/2) / Dyjj;
      }
      
      double pTj1, pTj2, pTgj, Zcent, pTll, mll, pTll_2, mll_2, Dyjj, mjj, Dphijj, pTbal;
      size_t Nj, Ngj;

      Jets getGapJets(const Jets& jets) {
        Jets gjets;
        if (jets.size() <= 2)  return gjets;
        FourMomentum j1 = jets[0].mom(), j2 = jets[1].mom();
        double yFwd = j1.rap(), yBwd = j2.rap();
        if (yFwd < yBwd) std::swap(yFwd,yBwd);
        for (size_t i = 2; i < jets.size(); ++i)
         if (inRange(jets[i].rap(), yBwd, yFwd)) gjets += jets[i];
        return gjets;
      }

    }; // struct variables

    struct Plots {
      string label;
      Histo1DPtr m_jj, Dphi_jj, Dy_jj, pT_ll, pT_ll_2, m_ll, m_ll_2;
    };


    void fillPlots(const Variables& vars, Plots& plots) {
      // The mjj plot extends down to 250 GeV
      plots.m_jj->fill(vars.mjj/GeV);
      //if (vars.mjj > 1000*GeV) {
      plots.Dy_jj->fill(vars.Dyjj);
      plots.Dphi_jj->fill(vars.Dphijj);
      plots.pT_ll->fill(vars.pTll/GeV);
      plots.pT_ll_2->fill(vars.pTll_2/GeV);
      plots.m_ll->fill(vars.mll/GeV);
      plots.m_ll_2->fill(vars.mll_2/GeV);
      //}
    }

    void scalePlots(Plots& plots, const double xsec){
      scale(plots.m_jj, xsec);
      scale(plots.Dy_jj, xsec);
      scale(plots.Dphi_jj, xsec);
      scale(plots.pT_ll, xsec);
      scale(plots.pT_ll_2, xsec);
      scale(plots.m_ll, xsec);
      scale(plots.m_ll_2, xsec);
    }

    //@}
    private:

      Plots mainPlot;
  };

  RIVET_DECLARE_PLUGIN(zz4ljjAnalysis);  
}
